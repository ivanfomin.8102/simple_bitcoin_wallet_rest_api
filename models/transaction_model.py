import uuid
from datetime import datetime
from sqlalchemy import Column, String, DateTime, Boolean, Float
from models.base_model import BaseModel


class Transaction(BaseModel):
    __tablename__ = 'transactions'

    transaction_id = Column(String(32), primary_key=True)
    amount = Column(Float, nullable=False)
    spent = Column(Boolean, nullable=False)
    created_at = Column(DateTime, nullable=False)

    def __init__(self, amount):
        self.transaction_id = uuid.uuid4().hex
        self.amount = amount
        self.spent = False
        self.created_at = datetime.now()
