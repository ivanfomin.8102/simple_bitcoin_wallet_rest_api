from backend.session_maker import SessionMaker


class BaseDAO:
    def __init__(self):
        self.connection = SessionMaker().session()
