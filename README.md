## Local development

* Python: 3.12+
* Other requirements could be seen in requirements.txt file 


1. Create a `.venv` file to contain all the required libraries.
2. Install dependencies (activate .venv first)

    ```shell
    pip install -r requirements.txt
    ```

3. Execute the `main.py` file to start the service (use this command)

    ```shell
    uvicorn main:app --host 127.0.0.1 --port 8000
    ```
   
4. Browser access: http://127.0.0.1:8000/docs

---

### Docker deploy

1. Use this command to create image for Docker (fast_api_project is a name, so it can be change):

   ```shell
    docker build -t fast_api_project .
    ```

2. Use Docker Desktop to create container.
3. Activate the container via Docker Desktop. 
4. Visit the browser: http://127.0.0.1:5000/docs (Dockerfile contains information about port and host, so if it will be necessary they could be changed.)
