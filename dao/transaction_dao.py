from sqlalchemy import select, update

from dao.base_dao import BaseDAO
from dto.transaction_entity import TransactionEntity
from models.transaction_model import Transaction


class TransactionDAO(BaseDAO):
    def __init__(self):
        super().__init__()

    async def get_transactions(self):
        async with self.connection as conn:
            query = select(Transaction)
            res = await conn.execute(query)
            result_orm = res.scalars().all()
            result_dto = [TransactionEntity.model_validate(row, from_attributes=True) for row in result_orm]
            return result_dto

    async def get_unspent_transactions(self):
        async with self.connection as conn:
            query = select(Transaction).where(Transaction.spent == False)
            res = await conn.execute(query)
            result_orm = res.scalars().all()
            result_dto = [TransactionEntity.model_validate(row, from_attributes=True) for row in result_orm]
            return result_dto

    async def create_transaction(self, amount: float):
        transaction = Transaction(amount=round(amount, 6))
        async with self.connection as conn:
            conn.add(transaction)
            await conn.flush()
            await conn.commit()

    async def update_transaction_by_id(self, transaction_id):
        async with self.connection as conn:
            query = update(Transaction).where(Transaction.transaction_id == transaction_id).values(spent=True)
            await conn.execute(query)
            await conn.flush()
            await conn.commit()

    async def update_transactions_by_id(self, transactions_id):
        async with self.connection as conn:
            query = update(Transaction).where(Transaction.transaction_id.in_(transactions_id)).values(spent=True)
            await conn.execute(query)
            await conn.flush()
            await conn.commit()

    # async def delete_transactions(self):
    #     async with (self.connection as conn):
    #         query = delete(Transaction).where(Transaction.transaction_id == Transaction.transaction_id)
    #         await conn.execute(query)
    #         await conn.flush()
    #         await conn.commit()
