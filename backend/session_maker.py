from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker

from models.base_model import Base


class SessionMaker:
    def __init__(self):
        url = f"sqlite+aiosqlite:///proj.db"
        self.engine = create_async_engine(url, echo=False, pool_recycle=3600)
        self.session = async_sessionmaker(bind=self.engine, expire_on_commit=False)

    async def create_tables(self):
        async with self.engine.begin() as conn:
            await conn.run_sync(Base.metadata.create_all)

    async def delete_tables(self):
        async with self.engine.begin() as conn:
            await conn.run_sync(Base.metadata.drop_all)
