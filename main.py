from contextlib import asynccontextmanager

import requests
from fastapi import FastAPI, Request, status, Body
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError, HTTPException
from fastapi.responses import JSONResponse

from backend.session_maker import SessionMaker
from dao.transaction_dao import TransactionDAO
from dto.transaction_entity import TransactionEntity

session_maker = SessionMaker()


@asynccontextmanager
async def lifespan(app: FastAPI):
    await session_maker.delete_tables()
    await session_maker.create_tables()
    yield


app = FastAPI(lifespan=lifespan)
Dao = TransactionDAO()


@app.get("/transactions", response_model=list[TransactionEntity])
async def get_transactions():
    transactions = await Dao.get_transactions()
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=jsonable_encoder(transactions)
    )


@app.get("/balance", response_model=dict[str, float])
async def get_balance():
    mult = float(requests.get("http://api-cryptopia.adca.sh/v1/prices/ticker").json()["data"][0]["value"])
    balance_btc = 0.0
    transactions = await Dao.get_unspent_transactions()
    for transaction in transactions:
        balance_btc += transaction.amount
    balance_eur = balance_btc * mult
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=jsonable_encoder({"Current balance in BTC": round(balance_btc, 6),
                                  "Current balance in EUR": round(balance_eur, 2)})
    )


@app.post("/transaction", response_model=dict[str, str])
async def create_transaction(amount: float = Body(...)):
    amount = await check_amount(amount)
    await Dao.create_transaction(amount)
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=jsonable_encoder({"status": "success"})
    )


@app.post("/transfer", response_model=dict[str, str])
async def transfer(amount: float = Body(...)):
    amount = await check_amount(amount)
    transactions = await Dao.get_unspent_transactions()
    transaction_id_list = []
    for transaction in transactions:
        amount -= transaction.amount
        transaction_id_list.append(transaction.transaction_id)
        if round(amount, 6) == 0.0:
            await Dao.update_transactions_by_id(transaction_id_list)
            break
        elif amount < 0.0:
            await Dao.update_transactions_by_id(transaction_id_list)
            await Dao.create_transaction(-amount)
            break

    if round(amount, 6) > 0:
        raise HTTPException(status_code=400, detail="Current balance does not have enough funds.")
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=jsonable_encoder({"status": "success"})
    )


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    return JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content=jsonable_encoder({"detail": exc.errors()}),
    )


@app.exception_handler(HTTPException)
async def validation_exception_handler(request: Request, exc: HTTPException):
    return JSONResponse(
        status_code=exc.status_code,
        content=jsonable_encoder({"detail": exc.detail}),
    )


async def check_amount(amount):
    if (amount * 100) % 1 != 0:
        raise RequestValidationError("Amount field must contain 2 digits after the decimal point.")
    mult = float(requests.get("http://api-cryptopia.adca.sh/v1/prices/ticker").json()["data"][0]["value"])
    amount = amount / mult
    if amount < 0.00001:
        raise RequestValidationError("Amount field is lower than 0.00001 BTC.")
    return amount
