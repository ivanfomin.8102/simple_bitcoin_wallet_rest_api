from starlette.testclient import TestClient

from main import app


def test_read_balance():
    with TestClient(app) as client:
        response = client.get("/balance")
        assert response.status_code == 200
        assert response.json() == {"Current balance in BTC": 0.0,
                                   "Current balance in EUR": 0.0}


def test_read_transactions():
    with TestClient(app) as client:
        response = client.get("/transactions")
        assert response.status_code == 200
        assert response.json() == []


def test_transfer_not_enough_funds():
    with TestClient(app) as client:
        response = client.post("/transfer", content="30.00")
        assert response.status_code == 400
        response = client.post("/transfer", content="30000.00")
        assert response.status_code == 400
        response = client.post("/transfer", content="1.00")
        assert response.status_code == 400


def test_transfer_more_than_2_digits():
    with TestClient(app) as client:
        response = client.post("/transfer", content="30.00123123")
        assert response.status_code == 422
        response = client.post("/transfer", content="0.00123123")
        assert response.status_code == 422
        response = client.post("/transfer", content="0.0003")
        assert response.status_code == 422
        response = client.post("/transfer", content="10.0000000")
        assert response.status_code == 400


def test_transfer_too_small():
    with TestClient(app) as client:
        response = client.post("/transfer", content="0.01")
        assert response.status_code == 422


def test_transaction_too_small():
    with TestClient(app) as client:
        response = client.post("/transaction", content="0.01")
        assert response.status_code == 422


def test_transaction_more_than_2_digits():
    with TestClient(app) as client:
        response = client.post("/transaction", content="0.01123132")
        assert response.status_code == 422
        response = client.post("/transaction", content="0.000001")
        assert response.status_code == 422
        response = client.post("/transaction", content="10.000")
        assert response.status_code == 200


def test_transaction():
    with TestClient(app) as client:
        response = client.post("/transaction", content="30.00")
        assert response.status_code == 200

        response = client.get("/transactions")
        assert response.status_code == 200
        assert len(response.json()) == 1

        response = client.get("/balance")
        assert response.status_code == 200
        assert response.json()["Current balance in EUR"] != 0.0


def test_transfer():
    with TestClient(app) as client:
        transaction_setup(client)
        response = client.get("/transactions")
        assert response.status_code == 200
        assert len(response.json()) == 2

        response = client.post("/transfer", content="35.00")
        assert response.status_code == 200

        response = client.get("/transactions")
        assert response.status_code == 200
        assert len(response.json()) == 3


def test_transfer_not_enough_funds_advanced():
    with TestClient(app) as client:
        transaction_setup(client)
        response = client.get("/transactions")
        assert response.status_code == 200
        assert len(response.json()) == 2

        response = client.post("/transfer", content="350.00")
        assert response.status_code == 400

        response = client.get("/transactions")
        assert response.status_code == 200
        assert len(response.json()) == 2


def test_transfer_extra():
    with TestClient(app) as client:
        for num in range(20):
            transaction_setup(client)

        response = client.post("/transfer", content="500.00")
        assert response.status_code == 200

        response = client.get("/transactions")
        assert response.status_code == 200
        assert len(response.json()) == 41


def test_transfer_not_enough_funds_extra():
    with TestClient(app) as client:
        for num in range(20):
            transaction_setup(client)

        response = client.post("/transfer", content="1350.00")
        assert response.status_code == 400

        response = client.get("/transactions")
        assert response.status_code == 200
        assert len(response.json()) == 40


def test_transfer_equal_transactions():
    with TestClient(app) as client:
        transaction_setup(client)

        response = client.post("/transfer", content="40.00")
        assert response.status_code == 200

        response = client.get("/transactions")
        assert response.status_code == 200
        assert len(response.json()) == 2


def transaction_setup(client):
    response = client.post("/transaction", content="30.00")
    assert response.status_code == 200
    response = client.post("/transaction", content="10.00")
    assert response.status_code == 200
