from sqlalchemy.ext.asyncio import AsyncAttrs
from sqlalchemy.orm import declarative_base

Base = declarative_base()


class BaseModel(AsyncAttrs, Base):
    __abstract__ = True
