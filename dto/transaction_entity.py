import uuid
from datetime import datetime

from pydantic import Field

from dto.base_entity import BaseEntity


class TransactionEntity(BaseEntity):
    transaction_id: str = Field(default=uuid.uuid4().hex, description="Id of the transaction.", max_length=32)
    amount: float = Field(default=0.0, description="Amount of money in BTC.", ge=0)
    spent: bool = Field(default=False, description="Transaction is spent ot not.")
    created_at: datetime = Field(default=datetime.now(), description="Time when the transaction was created.")
